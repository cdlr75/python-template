python-template
===============

Template for **sample** python3.+ package.

Includes:
- versioneer
- quality tests: pep8, pylint

Requirements
------------
Install `cookiecutter` command line: `pip install cookiecutter`    

Usage
-----
Generate a new Cookiecutter template layout: `cookiecutter gl:cdlr75/python-template`

License
-------
This project is licensed under the terms of the [MIT License](/LICENSE)
